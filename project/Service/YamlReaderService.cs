﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Service
{
    class YamlReaderService : ReaderService
    {
        public List<Product> ReadFile(Setting setting)
        {
            using (var input = File.OpenText(setting.Path))
            {
                List<Product> products = new List<Product>();
                var deserializerBuilder = new DeserializerBuilder().WithNamingConvention(new CamelCaseNamingConvention());
                var deserializer = deserializerBuilder.Build();
                dynamic tempProduct = deserializer.Deserialize<dynamic>(input);
                foreach (var items in tempProduct)
                {
                    Product product = new Product();
                    foreach (var item in items)
                    {
                        if (item.Key == setting.DefaultConfig.Name)
                            product.Name = item.Value;
                        else if (item.Key == setting.DefaultConfig.Tags)
                            product.Tags = item.Value;
                        else if (item.Key == setting.DefaultConfig.Twitter)
                            product.Twitter = item.Value;
                    }
                    Console.WriteLine("importing: Name: " + product.Name + "; Categories: " + product.Tags + "; Twitter: " + product.Twitter);
                    products.Add(product);
                }

                return products;
            }
        }
    }
}
