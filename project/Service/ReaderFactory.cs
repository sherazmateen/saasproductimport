﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ReaderFactory
    {
        public static ReaderService GetReaderServiceInstance(string type)
        {
            if (type == "yaml")
                return new YamlReaderService();
            if (type == "json")
                return new JsonReaderService();
            else
                return null;
        }
    }
}
