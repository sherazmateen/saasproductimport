﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Service
{
    class JsonReaderService : ReaderService
    {
        public List<Product> ReadFile(Setting setting)
        {
            List<Product> products = new List<Product>();
            using (StreamReader reader = new StreamReader(setting.Path))
            {
                var json = reader.ReadToEnd();
                dynamic tempProduct = JsonConvert.DeserializeObject<dynamic>(json);
                var tempProductList = tempProduct.products;
                foreach (var item in tempProductList)
                {
                    Product product = new Product();

                    product.Name = item[setting.DefaultConfig.Name];
                    product.Tags = String.Join(", ", item[setting.DefaultConfig.Tags]);
                    product.Twitter = item[setting.DefaultConfig.Twitter];
                    Console.WriteLine("importing: Name: " + product.Name + "; Categories: " + product.Tags + "; Twitter: " + product.Twitter);
                    products.Add(product);
                }
            }
            return products;
        }
    }
}
