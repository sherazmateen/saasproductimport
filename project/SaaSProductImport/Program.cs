﻿using Data;
using Model;
using Newtonsoft.Json;
using Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SaaSProductImport
{
    class Program
    {
        private static IDataAccess dataAccess;
        private static ReaderService readerService;
        private static Setting setting;
        private static List<ProductConfiguration> configurations;

        static void Main(string[] args)
        {
            //Following line is for debugging purpose only
            //System.Diagnostics.Debugger.Break();

            try
            {
                ReadConfigFile();
                if (IsValidArguments(args))
                {
                    InitializeSettings(args);

                    readerService = ReaderFactory.GetReaderServiceInstance(setting.DefaultConfig.Type);
                    List<Product> products = readerService.ReadFile(setting);

                    /*commenting the line below since it's not working in release mode 
                    but the idea is to instantiate the persistence object from "App.config" file*/

                    //dataAccess = DataAccessFactory.GetDataAccessInstance(ConfigurationManager.AppSettings["DataPersistenceOption"]);
                    dataAccess = DataAccessFactory.GetDataAccessInstance("File");

                    dataAccess.Save(products);
                }
            }
            catch (Exception e)
            {
                throw new Exception("An error occured in the program. Please refer to the message below: \n" + e);
            }
        }

        private static void InitializeSettings(string[] args)
        {
            try
            {
                setting = new Setting();
                setting.Command = args[0];
                setting.Client = args[1];
                setting.Path = args[2];
                setting.DefaultConfig = configurations.Single(config => config.Client == setting.Client);

            }
            catch (Exception e)
            {
                throw new Exception("Invalid command arguments!");
            }
        }

        private static bool IsValidArguments(string[] args)
        {
            try
            {
                if (args.Length != 3)
                {
                    return false;
                }
                else
                {
                    if ("import".Equals(args[0].ToLower()))
                    {
                        if (File.Exists(args[2]))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Invalid command arguments!");
            }
        }

        private static void ReadConfigFile()
        {
            using (StreamReader reader = new StreamReader("mappingConfig.json"))
            {
                var json = reader.ReadToEnd();
                configurations = JsonConvert.DeserializeObject<List<ProductConfiguration>>(json);
            }
        }

    }
}
