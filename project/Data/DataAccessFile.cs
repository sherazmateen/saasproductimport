﻿using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    class DataAccessFile : IDataAccess
    {
        public bool Save(List<Product> products)
        {
            using (StreamReader reader = new StreamReader("data.json"))
            {
                var json = reader.ReadToEnd();
                var fileProducts = JsonConvert.DeserializeObject<List<Product>>(json);
                if (fileProducts != null && fileProducts.Count != 0)
                {
                    foreach (Product product in fileProducts)
                    {
                        products.Add(product);
                    }
                }
            }

            using (StreamWriter file = File.CreateText("data.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, products);
            }

            return true;
        }
    }
}
