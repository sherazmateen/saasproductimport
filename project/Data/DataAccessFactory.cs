﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class DataAccessFactory
    {
        public static IDataAccess GetDataAccessInstance(string type)
        {
            if (type == "RDB")
                return new DataAccessRdb();
            else if (type == "NoSQL")
                return new DataAccessNosql();
            else if (type == "File")
                return new DataAccessFile();
            else
                return null;
        }
    }
}
