﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Product
    {
        public string Tags { get; set; }
        public string Name { get; set; }
        public string Twitter { get; set; }
    }
}
