﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Setting
    {
        public string Command { get; set; }
        public string Client { get; set; }
        public ProductConfiguration DefaultConfig { get; set; }
        public string Path { get; set; }
    }
}
