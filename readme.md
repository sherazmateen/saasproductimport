# SaaS Product Import #
The application reads data from client files in different formats and stores it in a json file. The applicaion is built in C# using Visual Studio 2017 Preview IDE.

## Getting started ##
- Please find the bitbucket link to the project at the following url: https://sherazmateen@bitbucket.org/sherazmateen/saasproductimport.git
- Please note that in order to run the project itself Visual Studio or any supporting software is **NOT** required. However to run the test suite the user must have Visual Studio installed.

## Running the application ##
- Please follow the steps below to run the application:
    - Open cmd  and navigate to following directory: /publish.
    - Run the "SaasProductImport.exe" file along with 3 space separated command line arguments i.e. SaaSProductImport.exe arg1 arg2 arg3
    - An example of valid command is:
````bash  
SaaSProductImport.exe import capterra "feed-products/capterra.yaml"
````
- Running the above command will not only show the desired output on cmd console but it will also store the output in data.json file which can be found in the same directory.

## Application structure ##
- The main solution is divied into 5 projects to keep each module separate namely:
    - SaaSProductImport: This project contains Program.cs file which has main function which is the entry point to our application
    - Data: This project holds all the persistence or storage layer files.
    - Model: This project holds all the models used in the application
    - Service: This project has all the service layer files.
    - SaaSProductImport.UnitTests: This project holds the unit test files.

## Running Unit Tests ##
- As mentioned above running unit tests requires Visual Studio installed on the user machine. To run the tests: 
    - Open the solution in Visual Studio
    - Build the application 
    - Go to Test->Run->Selected Tests/All Tests or simply hit Ctrl+R, A
- The test results can be seen in Test Explorer which can be accessed from Test->Windows->Test Explorer.

## Assumptions ##
- In creating the application I have assumed I can look at the contents of each file and update my "/publish/mappingConfig.json" file based on that.
- I use mappingConfig.json file to initialize objects which in turn instantiate required methods.
- Decoupling and Reusiblity have been kept in mind in designing the project in order to make it more flexible.