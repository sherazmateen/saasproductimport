﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Service;

namespace SaaSProductImport.UnitTests
{
    [TestClass]
    public class YamlReaderServiceTests
    {
        Setting setting;

        [TestMethod]
        public void ReadFile_TakesSettingObject_ReturnsProductList()
        {
            PopulateSetting();

            var readerService = ReaderFactory.GetReaderServiceInstance(setting.DefaultConfig.Type);

            var result = readerService.ReadFile(setting);

            Assert.IsInstanceOfType(result, typeof(List<Product>));
        }

        private void PopulateSetting()
        {
            ProductConfiguration productConfiguration = new ProductConfiguration();
            productConfiguration.Client = "capterra";
            productConfiguration.Name = "Name";
            productConfiguration.Tags = "Tags";
            productConfiguration.Twitter = "Twitter";
            productConfiguration.Type = "yaml";

            setting = new Setting();
            setting.Client = "capterra";
            setting.Command = "import";
            setting.Path = "feed-products/capterra.yaml";
            setting.DefaultConfig = productConfiguration;
        }
    }
}
