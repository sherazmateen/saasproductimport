﻿using System;
using System.Collections.Generic;
using Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace SaaSProductImport.UnitTests
{
    [TestClass]
    public class DataAccessFileTests
    {
        List<Product> products;

        [TestMethod]
        public void Save_TakesProductList_ReturnsTrue()
        {
            PopulateProductList();

            var dataAccess = DataAccessFactory.GetDataAccessInstance("File");

            var result = dataAccess.Save(products);

            Assert.IsTrue(result);
        }

        private void PopulateProductList()
        {
            Product product = new Product();
            product.Name = "someclient";
            product.Tags = "Tage One, Tag Two, Tag Three";
            product.Twitter = "@someclient";

            products = new List<Product>();
            products.Add(product);
        }
    }
}
